resource "kubernetes_service" "service_api-gateway" {
  metadata {
    labels = {
      app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
    }
    name = "api-gateway"
    namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
    }
    type = "ClusterIP"

    port {
      name = "api-gateway"
      port = 80
      target_port = 80
    }
  }
}

resource "kubernetes_service" "service_profiles-api" {
  metadata {
    labels = {
      app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
    }
    name = "profiles-api"
    namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
    }
    type = "ClusterIP"

    port {
      name = "profiles-api"
      port = 80
      target_port = 80
    }
  }
}
