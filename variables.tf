variable "keyvault_name" {
  default = "kwetter"
}

variable "resourcegroup_name" {
  default = "kwetter"
}

variable "registry_server" {
  default = "registry.gitlab.com"
}

variable "registry_username" {
  default = "registry-token"
}
