terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }

    helm = {
      source = "hashicorp/helm"
    }

    azurerm = {
      source  = "hashicorp/azurerm"
    }

  }
}

provider "kubernetes" {
  config_path = "C:/Users/Ensar Ishakoglu/.kube/config"
}

provider "azurerm" {
  features {}
}

provider "helm" {
  kubernetes {
    config_path = "C:/Users/Ensar Ishakoglu/.kube/config"
  }
}

//provider "mysql" {
//  endpoint = "database:3306"
//  username = "admin"
//  password = "admin"
//}

//provider "docker" {
//  registry_auth {
//    address = "registry.hub.docker.com"
//    config_file = pathexpand("~/.docker/config.json")
//  }
//
//  registry_auth {
//    address = "registry.gitlab.com"
//    config_file_content = <<DOCKER
//{
//  "auths": {
//    "${var.registry_server}": {
//      "auth": "${base64encode("${var.registry_username}:${data.azurerm_key_vault_secret.gitlab_registry_token.value}")}"
//    }
//  }
//}
//DOCKER
//  }
//
//  registry_auth {
//    address = "quay.io:8181"
//    username = "someuser"
//    password = "somepass"
//  }
//}
