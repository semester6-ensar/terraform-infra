resource "kubernetes_deployment" "deployment_api-gateway" {
  depends_on = [helm_release.rabbitmq, helm_release.database, helm_release.ingress_nginx]
  metadata {
    name = "kwetter-deployment"
    namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name
    labels = {
      app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
      }
    }
    template {
      metadata {
        labels = {
          app = kubernetes_namespace.kwetter_namespace.metadata.0.labels.app
        }
      }

      spec {
        hostname = "api-gateway"
        container {
          image = "registry.gitlab.com/semester6-ensar/api-gateway:latest"
          name = "api-gateway"

          port {
            container_port = 80
            host_port = 8080
          }

          env {
            name = "ASPNETCORE_ENVIRONMENT"
            value = "Production"
          }
        }

        image_pull_secrets {
          name = kubernetes_secret.gitlab_registry_token.metadata.0.name
        }
      }
    }
  }
}
