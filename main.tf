module "rabbitmq" {
  source = "rabbitmq"
}

resource "kubernetes_namespace" "kwetter_namespace" {
  metadata {
    labels = {
      app = "kwetter"
    }

    name = "kwetter-namespace"
  }
}

resource "helm_release" "ingress_nginx" {
  chart = "ingress-nginx"
  name = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  version = "3.15.2"
  namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name
  timeout = 300

  values = [<<EOF
controller:
  admissionWebhooks:
    enabled: false
  electionID: ingress-controller-leader-internal
  ingressClass: nginx-kwetter-namespace
  podLabels:
    app: ingress-nginx
  service:
    annotations:
      service.beta.kubernetes.io/aws-load-balancer-type: nlb
      nginx.ingress.kubernetes.io/enable-cors: "true"
      nginx.ingress.kubernetes.io/cors-allow-origin: "*"
      nginx.ingress.kubernetes.io/cors-allow-methods: "PUT, GET, POST, OPTIONS"
  scope:
    enabled: true
rbac:
  scope: true
EOF
  ]
}

resource "kubernetes_ingress" "ingress" {
  metadata {
    labels = {
      app = "ingress-nginx"
    }
    name = "api-ingress"
    namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class": "nginx-kwetter-namespace"
    }
  }
  spec {
    rule {
      http {
        path {
          path = "/"
          backend {
            service_name = "api-gateway"
            service_port = "80"
          }
        }
      }
    }
  }
}

resource "helm_release" "rabbitmq" {
  repository = "https://charts.bitnami.com/bitnami"
  chart = "rabbitmq"
  name = "rabbitmq"
  namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name

  set {
    name = "fullnameOverride"
    value = "rabbitmq"
  }

  set {
    name = "auth.username"
    value = "guest"
  }

  set {
    name = "auth.password"
    value = "guest"
  }

  set {
    name = "auth.erlangCookie"
    value = "MyErlangCookie123"
  }
}

resource "helm_release" "database" {
  repository = "https://charts.bitnami.com/bitnami"
  chart = "mysql"
  name = "mysql-database"
  namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name

  set {
    name = "fullnameOverride"
    value = "database"
  }

  set {
    name = "auth.username"
    value = "admin"
  }

  set {
    name = "auth.password"
    value = "admin"
  }

  set {
    name = "auth.rootPassword"
    value = "toor"
  }

  set {
    name = "initdbScripts"
    value = <<EOF
database-init.sql:
  CREATE DATABASE IF NOT EXISTS posts;
  CREATE DATABASE IF NOT EXISTS profile;
  CREATE DATABASE IF NOT EXISTS tags;
  GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%';
EOF
  }
}

//resource "mysql_database" "profile_db" {
//  depends_on = [helm_release.database]
//  name = "profile"
//}
//
//resource "mysql_database" "posts_db" {
//  depends_on = [helm_release.database]
//  name = "posts"
//}
