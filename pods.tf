//resource "kubernetes_pod" "pod_api-gateway" {
//  depends_on = [helm_release.rabbitmq, helm_release.database, helm_release.ingress_nginx]
//  metadata {
//    name = "api-gateway"
//    namespace = "kwetter-namespace"
//    labels = {
//      app = "kwetter"
//    }
//  }
//  spec {
//    image_pull_secrets {
//      name = kubernetes_secret.gitlab_registry_token.metadata.0.name
//    }
//    container {
//      image = "registry.gitlab.com/semester6-ensar/api-gateway:latest"
//      name = "api-gateway"
//
//      port {
//        container_port = 80
//        host_port = 80
//      }
//
//      env {
//        name = "ASPNETCORE_ENVIRONMENT"
//        value = "Production"
//      }
//    }
//  }
//}
//
//resource "kubernetes_pod" "pod_profiles-api" {
//  depends_on = [helm_release.rabbitmq, helm_release.database, helm_release.ingress_nginx]
//  metadata {
//    name = "profiles-api"
//    namespace = "kwetter-namespace"
//    labels = {
//      app = "kwetter"
//    }
//  }
//  spec {
//    image_pull_secrets {
//      name = kubernetes_secret.gitlab_registry_token.metadata.0.name
//    }
//    container {
//      image = "registry.gitlab.com/semester6-ensar/profile-api:latest"
//      name = "profiles-api"
//
//      port {
//        container_port = 80
//        host_port = 80
//      }
//    }
//  }
//}
