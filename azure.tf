data "azurerm_client_config" "current" {}

data "azurerm_key_vault" "kwetter_key_vault" {
  name                        = var.keyvault_name
  resource_group_name         = var.resourcegroup_name
}

data "azurerm_key_vault_secret" "gitlab_registry_token" {
  name         = "gitlab-registry-token"
  key_vault_id = data.azurerm_key_vault.kwetter_key_vault.id
}
