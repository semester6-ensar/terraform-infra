resource "kubernetes_secret" "gitlab_registry_token" {
  metadata {
    name = "registry-token"
    namespace = kubernetes_namespace.kwetter_namespace.metadata.0.name
  }

  data = {
    ".dockercfg" = "${file("${path.module}/docker-registry.json")}"
  }

    type = "kubernetes.io/dockercfg"
}
